void main() {
  var mtn = Awards();

  mtn.appName = "FNB";
  mtn.category = "Best ios";
  mtn.developerName = "FNB Connect";
  mtn.year = 2012;

  mtn.printAwardsInformation();

  mtn.appName = "SNAPSCAN";
  mtn.category = "Best HTML 5 App";
  mtn.developerName = "Kobus Ehlers";
  mtn.year = 2013;

  mtn.printAwardsInformation();

  mtn.appName = "Live Inspect";
  mtn.category = "Best android App";
  mtn.developerName = "Lightstone Auto & CustomApp";
  mtn.year = 2014;

  mtn.appName = "Wumdrop";
  mtn.category = "Best enterprise";
  mtn.developerName = "Benjamin Classen & Muneeb Samuels";
  mtn.year = 2015;

  mtn.printAwardsInformation();

  mtn.appName = "Domestly";
  mtn.category = "Best consumer App";
  mtn.developerName = "Berne Potgieter & Thatoyaona Marumo";
  mtn.year = 2016;

  mtn.printAwardsInformation();

  mtn.appName = "SHYFT";
  mtn.category = "Best financial solution";
  mtn.developerName = "Arno Von Helden";
  mtn.year = 2017;

  mtn.printAwardsInformation();

  mtn.appName = "Cowa bunga";
  mtn.category = "Best enterprise solution";
  mtn.developerName = "Kimberly Taylor & Bradley Rimmer";
  mtn.year = 2018;

  mtn.printAwardsInformation();

  mtn.appName = "Naked Insurance";
  mtn.category = "Best financial solution";
  mtn.developerName = "Sumarie Greybe, Ernest North & Alex Thomson";
  mtn.year = 2019;

  mtn.printAwardsInformation();

  mtn.appName = "EasyEquities";
  mtn.category = "Best consumer solution";
  mtn.developerName = "Charles Savage";
  mtn.year = 2020;

  mtn.printAwardsInformation();

  mtn.appName = "Ambani";
  mtn.category = "Best South African solution";
  mtn.developerName = "Mukundi Lambani";
  mtn.year = 2021;

  mtn.printAwardsInformation();
}

class Awards {
  String? appName;
  String? category;
  String? developerName;
  int? year;

  void printAwardsInformation() {
    print(appName?.toUpperCase());
    print("$category");
    print("$developerName");
    print("$year");
  }
}
